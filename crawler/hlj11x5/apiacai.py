from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    hlj11x5 = {}
    url = "https://www.acaicp.com/Ajax/10001"
    payload = {'LotIds':111023}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    errorCode = jsontext['Error']
    if errorCode == 'success':
        parstr = json.loads(jsontext['Value'])
        periods = str(parstr['Hlj11x5']['OpenDetail']['TermNo'])
        ymd = "20" + periods[:6]
        num = periods[-2:]
        hlj11x5['periods'] = ymd + '0' + num

        get_nums = parstr['Hlj11x5']['OpenDetail']['ArrayNum']
        hlj11x5['numbers'] = list(map(str, get_nums))

    print(url[:20],hlj11x5)
    return hlj11x5

if __name__ == "__main__":
    crawler()