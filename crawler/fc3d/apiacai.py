from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    fc3d_item = {}
    url = "https://www.acaicp.com/Ajax/10001"
    payload = {'LotIds':203001}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    errorCode = jsontext['Error']
    if errorCode == 'success':
        parstr = json.loads(jsontext['Value'])
        periods = str(parstr['Fc3d']['OpenDetail']['TermNo'])
        ymd = "20"
        num = periods
        fc3d_item['periods'] = ymd + num

        get_nums = parstr['Fc3d']['OpenDetail']['ArrayNum']
        fc3d_item['numbers'] = list(map(str, get_nums))

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()