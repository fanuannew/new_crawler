## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re, datetime

def crawler():
    fc3d_item = {}
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
                }

    url = "https://c8.cn/Record/OpenRecord?lType=1"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")

    # 期數'2018001'
    get_periods = soup.select_one('div.KJ_text dl:nth-of-type(1) dt').text
    # print(get_periods)
    finder = re.compile('\d{2,4}年')
    dates = finder.findall(get_periods)
    year = '{0:0>4}'.format(dates[0][0:-1])
    # finddate = re.compile('\d{1,2}日')
    # new_date = finddate.findall(get_periods)
    # day = '{0:0>2}'.format(new_date[0][0:-1])
    today = year
    fc3d_item['periods'] = today + '{0:0>3}'.format(get_periods[-3:-1])
    # 開獎號碼[1,9,5]
    get_nums = soup.select('div.KJ_text dl:nth-of-type(1) dd div span')
    # print(get_nums)
    fc3d_item['numbers'] = [n.text for n in get_nums]
     # 回傳格式
    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()