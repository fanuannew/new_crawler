from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    fc3d_item = {}
    url = "http://18881111.com/getDayData.ashx?lotCode=10041"
    resp = requests.get(url=url,timeout=10)
    jsontext = json.loads(resp.text)
    errorCode = jsontext['errorCode']
    if errorCode == 0:
        periods = str(jsontext['result']['data'][0]['preDrawIssue'])
        # ymd = "20" + periods[:6]
        # num = periods[-2:]
        fc3d_item['periods'] = str(periods)

        get_nums = jsontext['result']['data'][0]['preDrawCode']
        num_list = [str(x) for x in get_nums.split(',')]
        fc3d_item['numbers'] = list(map(str, num_list))

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()