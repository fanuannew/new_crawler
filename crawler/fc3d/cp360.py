## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re

def crawler():
    fc3d_item = {}
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
                }

    url = "http://chart.cp.360.cn/zst/sd?span=5&sortFlag=1"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")

    # 期數'2018001'
    periods = soup.select_one('#data-tab tr:nth-of-type(1) td:nth-of-type(3)').text
    fc3d_item['periods'] = periods.replace("-","")
    # 開獎號碼[1,9,5]
    get_nums = soup.select_one('#chart-tab table tbody tr:nth-of-type(1) td:nth-of-type(5) strong').text
    fc3d_item['numbers'] = list(get_nums)
     # 回傳格式
    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()