from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    fc3d_item = {}
    headers = {
                    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                }
    url = "https://www.ag3333.com/mobile/controller/getLotteryHistory.php"
    payload = {'date': 'date','type':'fc3d','czType':''}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext[0]['qishu'])
        # ymd = periods[:8]
        # num = periods[-2:]
        fc3d_item['periods'] = periods

        get_nums = jsontext[0]['jieguo']
        num_list = get_nums.split(',')
        fc3d_item['numbers'] = list(map(str, num_list))

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()