from bs4 import BeautifulSoup
import requests
import re, json, datetime

def crawler():
    hkmk6_item = {}
    year = datetime.datetime.today().strftime('%Y')
    url = "https://db.1988kai.com/gather/services/rest/api/QuanGuoCai/getLotteryInfoList.do?date=&lotCode=10041"
    # payload = {'year': year,'type':'1'}
    resp = requests.get(url=url,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext['result']['data'][0]['preDrawIssue'])
        # ymd = year
        # num = '{0:0>3}'.format(periods)
        hkmk6_item['periods'] = periods

        get_nums = jsontext['result']['data'][0]['preDrawCode']
        num_list = [str(x) for x in get_nums.split(',')]
        hkmk6_item['numbers'] = num_list

    print(url[:20],hkmk6_item)
    return hkmk6_item

if __name__ == "__main__":
    crawler()