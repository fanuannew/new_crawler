import pymysql
import requests
from .SQLfield import *
from .tablename import table_name
from ..config import config_set
import datetime

class Dbquery:
    def __init__(self):
        self.conn = pymysql.connect(config_set['MYSQL_HOST'], config_set['MYSQL_USER'], config_set['MYSQL_PASSWORD'], config_set['MYSQL_DBNAME'],
                                    charset='utf8', use_unicode=True)
        self.cursor = self.conn.cursor()

    # 查詢資料庫期數
    def querylist(self,sql):
        self.cursor.execute(sql)
        result = self.cursor.fetchall()
        return result

    # 更新資料庫
    def update_db(self,con,item): # con:彩種, item:彩種的內容
        # 設定彩種table名稱
        tableName = table_name[con]

        # SQL格式設定, 要插入的dict： 字段：值
        data_dict = SQL_Fields(con).get_fields(item['periods'], item['winning_numbers'])
        # 呼叫同個class下的整理SQL def.
        sql_statement = self.get_sql_update(tableName, data_dict)
        print(sql_statement)
        try:
            self.cursor.execute(sql_statement)
            self.conn.commit()
            # ex.active_crontab('hnkl10',item['periods'])
            ms = self.active_crontab(con, item['periods'])
            print('呼叫MQ更新狀態: ', ms)
            print('資料庫更新成功, 彩種: ', str(con))
        except:
            print('資料庫更新失敗，無法插入資料, 彩種: ', str(con))
    
    # 呼叫MQ更新開獎
    def active_crontab(self,con,qishu):  
        url = config_set['MQ_URL']+'?key_word='+con+'&qishu=' + qishu
        r = requests.get(url)
        return r

    # 製作"最終"更新資料庫的SQL語法
    def get_sql_update(self,table,data):
        sql_statement = 'UPDATE ' + table + ' SET '
        tempsql = ''
        for key in data:
            if key == 'qishu' :
                continue
            tempsql = tempsql + key + '="' + data[key]+'",'

        tempsql = tempsql.strip(',')
        sql_statement = sql_statement + tempsql + ' WHERE qishu="'+data['qishu']+'" AND status="0"'
        return sql_statement

    # 查詢資料庫有無開獎資料存在
    def check_sql_updated(self, con, qishu): #class.self, 彩種簡稱, 期數
        sql = " select * from " + table_name[con] + " where qishu='"+str(qishu)+"' and status=1"
        # 等1.5秒再去檢查資料庫
        time.sleep( 1.5 )
        self.cursor.execute(sql)
        result = self.cursor.fetchall()
        if len(result) <1 :
            print("查無此 ",qishu,"期開獎資料")
        else:
            print("資料庫開獎檢驗: ", result)
        return result

    # 查詢資料庫還沒開獎的最舊期數(準備要開)的期數
    #class.self, 彩種簡稱, 是否切換成前天, 時間(期數)格式
    def check_sql_toopen(self, con, yester_d, format_d): 
        # 產生不同期數的格式
        form_date = ''
        if format_d =='YM':
            form_date = '%Y%m'
        elif format_d == 'Y':
            form_date = '%Y'
        elif format_d == 'YMD':
            form_date = "%Y%m%d"
        elif format_d == 'qishu':
            pass
        keywds = datetime.datetime.today().strftime(form_date)
        # 如果是當天但是期數號碼是昨天的情況，記得設成True進行判斷
        if yester_d == True:
            yes_time = datetime.datetime.today() + datetime.timedelta(days=-1)
            keywds = yes_time.strftime(form_date)
        # print(keywds)
        sql = "select id, qishu from " + table_name[con] + ' where qishu LIKE "%'+ keywds +'%" and status=0 and qishu > (select qishu from ' + table_name[con] + ' where status=1 order by id desc limit 1) order by qishu limit 1'
        if format_d == 'qishu':
            sql = "select id, qishu from " + table_name[con] + ' where status=0 and qishu > (select qishu from ' + table_name[con] + ' where status=1 order by id desc limit 1) order by qishu limit 1'
        # print(sql)
        self.cursor.execute(sql)
        result = self.cursor.fetchall()
        for row in result:
            id = row[0]
            period = row[1]
        if len(result) <1 :
            print("查最新準備要開獎的資料")
        else:
            print("資料庫準備要開獎的期數: ",period,"::", result)
        return period

