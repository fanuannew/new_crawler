import time

class SQL_Fields:
    # 初始化並依照彩種名稱導入def路徑
    def __init__(self, name):
        self.con = name

    # 引導器, 引導到彩種對應的def
    def get_fields(self, periods, winning_numbers):
        result = ''
        # print('SQL彩種產生器!:::::::::::',self.con)
        easy_get = ['gd11x5', 'sd11x5', 'jx11x5', 'sh11x5', 'xj11x5', 'js11x5', 'ln11x5', 'heb11x5', 'hlj11x5', 'hkmk6']
        # if self.con == 'gd11x5' or self.con == 'sd11x5' or self.con == 'jx11x5':
        #     result = self.universal_get_fields(periods, winning_numbers)
        for x in easy_get :
            if self.con == x:
                result = self.universal_get_fields(periods, winning_numbers)
        # 北京pc28
        if self.con == 'bjpc28':
            result = self.bjpc28_get_fields(periods, winning_numbers)
        # 北京PK10
        elif self.con == 'bjpk10':
            result = self.bjpk10_get_fields(periods, winning_numbers)
        # 加拿大PC28
        elif self.con == 'canadapc28':
            result = self.canadapc28_get_fields(periods, winning_numbers)
        # 福彩3D
        elif self.con == 'fc3d':
            result = self.fc3d_get_fields(periods, winning_numbers)
        # 排列3
        elif self.con == 'pl3':
            result = self.pl3_get_fields(periods, winning_numbers)
        # 广西快3
        elif self.con == 'gxk3':
            result = self.gxk3_get_fields(periods, winning_numbers)
        # 湖南快十
        elif self.con == 'hnkl10':
            result = self.hnkl10_get_fields(periods, winning_numbers)
        # 江苏快3
        elif self.con == 'jsk3':
            result = self.jsk3_get_fields(periods, winning_numbers)
        # 上海快3
        elif self.con == 'shk3':
            result = self.shk3_get_fields(periods, winning_numbers)
        # 天津快十
        elif self.con == 'tjkl10':
            result = self.tjkl10_get_fields(periods, winning_numbers)
        # 重庆时时彩
        elif self.con == 'cqssc':
            result = self.cqssc_get_fields(periods, winning_numbers)
        # 天津时时彩
        elif self.con == 'tjssc':
            result = self.tjssc_get_fields(periods, winning_numbers)
        # 新疆时时彩
        elif self.con == 'xjssc':
            result = self.xjssc_get_fields(periods, winning_numbers)
        # 幸运飞艇
        elif self.con == 'xyft':
            result = self.xyft_get_fields(periods, winning_numbers)
        # 極速快10
        elif self.con == 'jisukl10':
            result = self.jisukl10_get_fields(periods, winning_numbers)
        # 極速pk10
        elif self.con == 'jisupk10':
            result = self.jisupk10_get_fields(periods, winning_numbers)
        # 極速pc28
        elif self.con == 'jisupc28':
            result = self.jisupc28_get_fields(periods, winning_numbers)
        # 極速3D
        elif self.con == 'jisu3d':
            result = self.jisu3d_get_fields(periods, winning_numbers)
        return result

    # 設定[通用]只抓期數跟開獎結果的SQL格式
    # 廣東11選5欄位 gd11x5
    # 山東11選5欄位 sd11x5
    # 江西11選5欄位 jx11x5
    # 上海11選5欄位 sh11x5
    # 新疆11選5欄位 xj11x5
    # 江蘇11選5欄位 js11x5
    # 遼寧11選5欄位 ln11x5
    # 河北11選5欄位 heb11x5
    # 黑龍江11選5欄位 hlj11x5
    # 香港六合彩 hkmk6
    def universal_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'2018061901'， 號碼 [1,2,3....]
        qishu = periods
        result = winning_numbers

        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data

    # 北京pc28字段
    def bjpc28_get_fields(self, periods, winning_numbers):

        # 傳入的數據： 期數'664871'， 號碼 [1,2,3....]

        qishu = periods
        result = winning_numbers

        numbers = result[:-1]
        numbers.sort()

        value_eight = "0"
        value_seven = "0"
        if len(set(numbers)) == 1:
            value_eight = "1"
        elif len(set(numbers)) == 2 :
            value_seven = "1"

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        data = {
            'qishu': qishu,
            'number_one': str(result[0]),
            'number_two': str(result[1]),
            'number_three': str(result[2]),
            'value_one': '1' if int(result[3]) > 13 else '0',
            'value_two': '1' if int(result[3]) % 2 != 0 else '0',
            'value_three': str(result[3]),
            'value_four': '1' if int(result[3]) > 13 and int(result[3]) % 2 != 0 else (
                "2" if int(result[3]) <= 13 and int(result[3]) % 2 != 0 else "0"),
            'value_five': '1' if int(result[3]) > 13 and int(result[3]) % 2 == 0 else (
                "2" if int(result[3]) <= 13 and int(result[3]) % 2 == 0 else "0"),
            'value_six': '2' if int(result[3]) <= 5 else ("1" if int(result[3]) >= 22 else '0'),
            'value_seven': value_seven,
            'value_eight': value_eight,
            'value_nine': '1' if result[0] > result[2] else ("0" if result[0] < result[2] else "2"),
            'value_ten': '1' if numbers[2] - numbers[0] == 2 and len(set(numbers)) == 3 else '0',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }

        return data

    def bjpk10_get_fields(self, periods, winning_numbers):

        # 傳入的數據： 期數'664871'， 號碼 [1,2,3....]

        qishu = periods

        result = winning_numbers

        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': '大' if (int(result[0]) + int(result[1])) > 11 else '小',
            'value_two': '单' if (int(result[0]) + int(result[1])) % 2 != 0 else '双',
            'value_three': str(result[0]),
            'value_four': '龙' if int(result[0]) > int(result[9]) else '虎',
            'value_five': '龙' if int(result[1]) > int(result[8]) else '虎',
            'value_six': '龙' if int(result[2]) > int(result[7]) else '虎',
            'value_seven': '龙' if int(result[3]) > int(result[6]) else '虎',
            'value_eight': '龙' if int(result[4]) > int(result[5]) else '虎',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }

        return data

    # 加拿大pc28插入字段
    def canadapc28_get_fields(self, periods, winning_numbers):

        # 傳入的數據： 期數'664871'， 號碼 [1,2,3....]
        qishu = periods
        result = winning_numbers

        numbers = result[:-1]
        numbers.sort()
        value_eight = "0"
        value_seven = "0"
        if len(set(numbers)) == 1:
            value_eight = "1"
        elif len(set(numbers)) == 2:
            value_seven = "1"
        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字段-------------
        data = {
            'qishu': qishu,
            'number_one': str(result[0]),
            'number_two': str(result[1]),
            'number_three': str(result[2]),
            'value_one': '1' if int(result[3]) > 13 else '0',
            'value_two': '1' if int(result[3]) % 2 != 0 else '0',
            'value_three': str(result[3]),
            'value_four': '1' if int(result[3]) > 13 and int(result[3]) % 2 != 0 else (
                "2" if int(result[3]) <= 13 and int(result[3]) % 2 != 0 else "0"),
            'value_five': '1' if int(result[3]) > 13 and int(result[3]) % 2 == 0 else (
                "2" if int(result[3]) <= 13 and int(result[3]) % 2 == 0 else "0"),
            'value_six': '2' if int(result[3]) <= 5 else ("1" if int(result[3]) >= 22 else '0'),
            'value_seven': value_seven,
            'value_eight': value_eight,
            'value_nine': '1' if result[0] > result[2]  else ("0" if result[0] < result[2] else "2"),
            'value_ten': '1' if numbers[2] - numbers[0] == 2 and len(set(numbers)) == 3 else '0',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }

        return data

    def cqssc_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'664871'， 號碼 [1,2,3....]

        qishu = periods
        result = winning_numbers

        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))

        value_one = "0"
        value_two = "0"
        value_three = "0"
        value_four = "0"
        value_five = "0"
        value_six = "0"
        value_seven = "0"
        value_eight = "0"
        value_nine = "0"
        value_ten = "0"
        value_eleven = "0"
        value_twelve = "0"
        value_thirteen = "0"
        value_fourteen = "0"
        value_fifteen = "0"
        # 前3
        qian3 = result[0:3]
        qian3.sort()

        #判斷前3是不是 豹子，順子，對子，半順，雜六
        if len(set(qian3)) == 1 :
            value_one = "1"
        elif (qian3[1] - qian3[0] == 1 and qian3[2] - qian3[1] == 1) or [0, 1,9] == qian3 or qian3 == [0,8,9] :
            value_two = "1"
        elif len(set(qian3)) == 2 :
            value_three = "1"
        elif qian3[1] - qian3[0] == 1 or qian3[2] - qian3[1] == 1 or qian3[2] - qian3[0] == 9 :
            value_four = "1"
        else:
            value_five = "1"
        # 中3
        zhong3 = winning_numbers[1:4]
        zhong3.sort()
        # 判斷中3是不是 豹子，順子，對子，半順，雜六
        if len(set(zhong3)) == 1:
            value_six = "1"
        elif (zhong3[1] - zhong3[0] == 1 and zhong3[2] - zhong3[1] == 1) or [0, 1, 9] == zhong3 or zhong3 == [0, 8, 9]:
            value_seven = "1"
        elif len(set(zhong3)) == 2:
            value_eight = "1"
        elif zhong3[1] - zhong3[0] == 1 or zhong3[2] - zhong3[1] == 1 or zhong3[2] - zhong3[0] == 9:
            value_nine = "1"
        else:
            value_ten = "1"
        # 後3
        hou3 = winning_numbers[2:5]
        hou3.sort()
        if len(set(hou3)) == 1:
            value_eleven = "1"
        elif (hou3[1] - hou3[0] == 1 and hou3[2] - hou3[1] == 1) or [0, 1, 9] == hou3 or hou3 == [0, 8, 9]:
            value_twelve = "1"
        elif len(set(hou3)) == 2:
            value_thirteen = "1"
        elif hou3[1] - hou3[0] == 1 or hou3[2] - hou3[1] == 1 or hou3[2] - hou3[0] == 9:
            value_fourteen = "1"
        else:
            value_fifteen = "1"
        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------

        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': value_one,
            'value_two': value_two,
            'value_three': value_three,
            'value_four': value_four,
            'value_five': value_five,
            'value_six': value_six,
            'value_seven': value_seven,
            'value_eight': value_eight,
            'value_nine': value_nine,
            'value_ten': value_ten,
            'value_eleven': value_eleven,
            'value_twelve': value_twelve,
            'value_thirteen': value_thirteen,
            'value_fourteen':  value_fourteen,
            'value_fifteen': value_fifteen,
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }

        return data

    # 福彩3D
    def fc3d_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'2018001'， 號碼 [1,2,3]
        qishu = periods
        result = winning_numbers

        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))
        # print('有執行福彩3D SQL')
        # -------------插入字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': '1' if int(result[2]) > 4 else '0',#個位 1大 0小
            'value_two': '1' if int(result[2]) % 2 != 0 else '0',#個位 1單 0雙
            'value_three': '1' if int(result[1]) > 4 else '0',#十位 1大 0小
            'value_four': '1' if int(result[1]) % 2 != 0 else '0',#十位 1單 0雙
            'value_five': '1' if int(result[0]) > 4 else '0',#百位 1大 0小
            'value_six': '1' if int(result[0]) % 2 != 0 else '0',#百位 1單 0雙
            'status': '1',
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data

    # 排列3
    def pl3_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'2018001'， 號碼 [1,2,3]
        qishu = periods
        result = winning_numbers

        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': '1' if int(result[2]) > 4 else '0',#個位 1大 0小
            'value_two': '1' if int(result[2]) % 2 != 0 else '0',#個位 1單 0雙
            'value_three': '1' if int(result[1]) > 4 else '0',#十位 1大 0小
            'value_four': '1' if int(result[1]) % 2 != 0 else '0',#十位 1單 0雙
            'value_five': '1' if int(result[0]) > 4 else '0',#百位 1大 0小
            'value_six': '1' if int(result[0]) % 2 != 0 else '0',#百位 1單 0雙
            'status': '1',
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data

    def gxk3_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'20180321010'， 號碼 [1,2,3....]
        qishu = periods
        result = winning_numbers
        winnerNumber = winning_numbers
        v3 = sum(result)
        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))
        # -------------插入字典-------------
        data = {
            'qishu': qishu,
            'number_one': str(result[0]),
            'number_two': str(result[1]),
            'number_three': str(result[2]),
            'value_one': '1' if v3 > 10 else '0',
            'value_two': '1' if v3 % 2 != 0 else '0',
            'value_three': str(v3),
            'value_four': '1' if len(set(result)) == 2 else "0",
            'value_five': '1' if winnerNumber[2] - winnerNumber[1] == 1 and winnerNumber[1] - winnerNumber[
                0] == 1 else "0",
            'value_six': '1' if result[0] == result[1] and result[1] == result[2] else '0',
            'value_seven': '1' if result[0] == result[1] or result[0] == result[2] or result[1] == result[2] else '0',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data

    def hnkl10_get_fields(self, periods, winning_numbers):

        # 傳入的數據： 期數'664871'， 號碼 [1,2,3....]
        qishu = periods
        result = winning_numbers
        # [1,2] -> '1,2'
        numbers = ','.join(list(map(str, winning_numbers)))
        hezhi = sum(result)
        hezhistr = str(hezhi)
        # 時間戳
        nowtime = str(int(time.time()))
        # -------------要插入的字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': '总大' if hezhi > 84 else ('总小' if hezhi < 84 else '和'),
            'value_two': '单' if hezhi % 2 != 0 else '双',
            'value_three': '尾大' if int(hezhistr[-1]) >= 5 else '尾小',
            'value_four': '龙' if result[0] > result[7] else '虎',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data

    def jsk3_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'20180321010'， 號碼 [1,2,3....]

        qishu = periods

        result = winning_numbers

        winnerNumber = winning_numbers

        v3 = sum(result)

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------

        data = {
            'qishu': qishu,
            'number_one': str(result[0]),
            'number_two': str(result[1]),
            'number_three': str(result[2]),
            'value_one': '1' if v3 > 10 else '0',
            'value_two': '1' if v3 % 2 != 0 else '0',
            'value_three': str(v3),
            'value_four': '1' if len(set(result)) == 2 else "0",
            'value_five': '1' if winnerNumber[2] - winnerNumber[1] == 1 and winnerNumber[1] - winnerNumber[0] == 1 else "0",
            'value_six': '1' if result[0] == result[1] and result[1] == result[2] else '0',
            'value_seven': '1' if result[0] == result[1] or result[0] == result[2] or result[1] == result[2] else '0',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }

        return data

    def shk3_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'2018032110'， 號碼 [1,2,3....]

        qishu = periods

        result = winning_numbers

        winnerNumber = winning_numbers

        v3 = sum(result)

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------

        data = {
            'qishu': qishu,
            'number_one': str(result[0]),
            'number_two': str(result[1]),
            'number_three': str(result[2]),
            'value_one': '1' if v3 > 10 else '0',
            'value_two': '1' if v3 % 2 != 0 else '0',
            'value_three': str(v3),
            'value_four': '1' if len(set(result)) == 2 else "0",
            'value_five': '1' if winnerNumber[2] - winnerNumber[1] == 1 and winnerNumber[1] - winnerNumber[
                0] == 1 else "0",
            'value_six': '1' if result[0] == result[1] and result[1] == result[2] else '0',
            'value_seven': '1' if result[0] == result[1] or result[0] == result[2] or result[1] == result[2] else '0',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }

        return data

    def tjkl10_get_fields(self, periods, winning_numbers):

        # 傳入的數據： 期數'20180702+01'， 號碼 [1,2,3...,20]
        qishu = periods
        result = winning_numbers
        # [1,2] -> '1,2'
        numbers = ','.join(list(map(str, winning_numbers)))
        hezhi = sum(result)
        hezhistr = str(hezhi)
        # 時間戳
        nowtime = str(int(time.time()))
        # -------------要插入的字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': '总大' if hezhi > 84 else ('总小' if hezhi < 84 else '和'),
            'value_two': '单' if hezhi % 2 != 0 else '双',
            'value_three': '尾大' if int(hezhistr[-1]) >= 5 else '尾小',
            'value_four': '龙' if result[0] > result[7] else '虎',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data

    def tjssc_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'664871'， 號碼 [1,2,3....]
        qishu = periods
        result = winning_numbers

        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))
        value_one = "0"
        value_two = "0"
        value_three = "0"
        value_four = "0"
        value_five = "0"
        value_six = "0"
        value_seven = "0"
        value_eight = "0"
        value_nine = "0"
        value_ten = "0"
        value_eleven = "0"
        value_twelve = "0"
        value_thirteen = "0"
        value_fourteen = "0"
        value_fifteen = "0"
        # 前3
        qian3 = result[0:3]
        qian3.sort()
        # 判斷前3是不是 豹子，順子，對子，半順，雜六
        if len(set(qian3)) == 1:
            value_one = "1"
        elif (qian3[1] - qian3[0] == 1 and qian3[2] - qian3[1] == 1) or [0, 1, 9] == qian3 or qian3 == [0, 8, 9]:
            value_two = "1"
        elif len(set(qian3)) == 2:
            value_three = "1"
        elif qian3[1] - qian3[0] == 1 or qian3[2] - qian3[1] == 1 or qian3[2] - qian3[0] == 9:
            value_four = "1"
        else:
            value_five = "1"
        # 中3
        zhong3 = winning_numbers[1:4]
        zhong3.sort()
        # 判斷中3是不是 豹子，順子，對子，半順，雜六
        if len(set(zhong3)) == 1:
            value_six = "1"
        elif (zhong3[1] - zhong3[0] == 1 and zhong3[2] - zhong3[1] == 1) or [0, 1, 9] == zhong3 or zhong3 == [0, 8, 9]:
            value_seven = "1"
        elif len(set(zhong3)) == 2:
            value_eight = "1"
        elif zhong3[1] - zhong3[0] == 1 or zhong3[2] - zhong3[1] == 1 or zhong3[2] - zhong3[0] == 9:
            value_nine = "1"
        else:
            value_ten = "1"
        # 後3
        hou3 = winning_numbers[2:5]
        hou3.sort()
        if len(set(hou3)) == 1:
            value_eleven = "1"
        elif (hou3[1] - hou3[0] == 1 and hou3[2] - hou3[1] == 1) or [0, 1, 9] == hou3 or hou3 == [0, 8, 9]:
            value_twelve = "1"
        elif len(set(hou3)) == 2:
            value_thirteen = "1"
        elif hou3[1] - hou3[0] == 1 or hou3[2] - hou3[1] == 1 or hou3[2] - hou3[0] == 9:
            value_fourteen = "1"
        else:
            value_fifteen = "1"
        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------

        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': value_one,
            'value_two': value_two,
            'value_three': value_three,
            'value_four': value_four,
            'value_five': value_five,
            'value_six': value_six,
            'value_seven': value_seven,
            'value_eight': value_eight,
            'value_nine': value_nine,
            'value_ten': value_ten,
            'value_eleven': value_eleven,
            'value_twelve': value_twelve,
            'value_thirteen': value_thirteen,
            'value_fourteen': value_fourteen,
            'value_fifteen': value_fifteen,
            # 'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }

        return data

    def xjssc_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'664871'， 號碼 [1,2,3....]
        qishu = periods
        result = winning_numbers
        value_one = "0"
        value_two = "0"
        value_three = "0"
        value_four = "0"
        value_five = "0"
        value_six = "0"
        value_seven = "0"
        value_eight = "0"
        value_nine = "0"
        value_ten = "0"
        value_eleven = "0"
        value_twelve = "0"
        value_thirteen = "0"
        value_fourteen = "0"
        value_fifteen = "0"
        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))
        # 前3
        qian3 = result[0:3]
        qian3.sort()
        #判斷前3是不是 豹子，順子，對子，半順，雜六
        if len(set(qian3)) == 1 :
            value_one = "1"
        elif (qian3[1] - qian3[0] == 1 and qian3[2] - qian3[1] == 1) or [0, 1,9] == qian3 or qian3 == [0,8,9] :
            value_two = "1"
        elif len(set(qian3)) == 2 :
            value_three = "1"
        elif qian3[1] - qian3[0] == 1 or qian3[2] - qian3[1] == 1 or qian3[2] - qian3[0] == 9 :
            value_four = "1"
        else:
            value_five = "1"
        # 中3
        zhong3 = winning_numbers[1:4]
        zhong3.sort()
        # 判斷中3是不是 豹子，順子，對子，半順，雜六
        if len(set(zhong3)) == 1:
            value_six = "1"
        elif (zhong3[1] - zhong3[0] == 1 and zhong3[2] - zhong3[1] == 1) or [0, 1, 9] == zhong3 or zhong3 == [0, 8, 9]:
            value_seven = "1"
        elif len(set(zhong3)) == 2:
            value_eight = "1"
        elif zhong3[1] - zhong3[0] == 1 or zhong3[2] - zhong3[1] == 1 or zhong3[2] - zhong3[0] == 9:
            value_nine = "1"
        else:
            value_ten = "1"
        # 後3
        hou3 = winning_numbers[2:5]
        hou3.sort()
        if len(set(hou3)) == 1:
            value_eleven = "1"
        elif (hou3[1] - hou3[0] == 1 and hou3[2] - hou3[1] == 1) or [0, 1, 9] == hou3 or hou3 == [0, 8, 9]:
            value_twelve = "1"
        elif len(set(hou3)) == 2:
            value_thirteen = "1"
        elif hou3[1] - hou3[0] == 1 or hou3[2] - hou3[1] == 1 or hou3[2] - hou3[0] == 9:
            value_fourteen = "1"
        else:
            value_fifteen = "1"
        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))
        # -------------插入字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': value_one,
            'value_two': value_two,
            'value_three': value_three,
            'value_four': value_four,
            'value_five': value_five,
            'value_six': value_six,
            'value_seven': value_seven,
            'value_eight': value_eight,
            'value_nine': value_nine,
            'value_ten': value_ten,
            'value_eleven': value_eleven,
            'value_twelve': value_twelve,
            'value_thirteen': value_thirteen,
            'value_fourteen': value_fourteen,
            'value_fifteen': value_fifteen,
            # 'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data
    
    def xyft_get_fields(self, periods, winning_numbers):

        # 傳入的數據： 期數'20180319133'， 號碼 [1,2,3....]

        qishu = periods

        result = winning_numbers

        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': '大' if (int(result[0]) + int(result[1])) > 11 else '小',
            'value_two': '单' if (int(result[0]) + int(result[1])) % 2 != 0 else '双',
            'value_three': str(result[0]),
            'value_four': '龙' if int(result[0]) > int(result[9]) else '虎',
            'value_five': '龙' if int(result[1]) > int(result[8]) else '虎',
            'value_six': '龙' if int(result[2]) > int(result[7]) else '虎',
            'value_seven': '龙' if int(result[3]) > int(result[6]) else '虎',
            'value_eight': '龙' if int(result[4]) > int(result[5]) else '虎',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data

    # 極速快10
    def jisukl10_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'2018001'， 號碼 [1,2,3]
        qishu = periods
        result = winning_numbers
        hezhi = sum(result)
        hezhistr = str(hezhi)

        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': '总大' if hezhi > 84 else ('总小' if hezhi < 84 else '和'),
            'value_two': '单' if hezhi % 2 != 0 else '双',
            'value_three': '尾大' if int(hezhistr[-1]) >= 5 else '尾小',
            'value_four': '龙' if result[0] > result[7] else '虎',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data

    # 極速pk10插入字段
    def jisupk10_get_fields(self, periods, winning_numbers):

        # 傳入的數據： 期數'20180702+0001'， 號碼 [1,2,3...,10]
        qishu = periods
        result = winning_numbers

        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': '大' if (int(result[0]) + int(result[1])) > 11 else '小',
            'value_two': '单' if (int(result[0]) + int(result[1])) % 2 != 0 else '双',
            'value_three': str(result[0]),
            'value_four': '龙' if int(result[0]) > int(result[9]) else '虎',
            'value_five': '龙' if int(result[1]) > int(result[8]) else '虎',
            'value_six': '龙' if int(result[2]) > int(result[7]) else '虎',
            'value_seven': '龙' if int(result[3]) > int(result[6]) else '虎',
            'value_eight': '龙' if int(result[4]) > int(result[5]) else '虎',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }

        return data

    # 極速pc28字段
    def jisupc28_get_fields(self, periods, winning_numbers):

        # 傳入的數據： 期數'20180702+0001'， 號碼 [1,2,3]
        qishu = periods
        result = winning_numbers

        numbers = result[:-1]
        numbers.sort()

        value_eight = "0"
        value_seven = "0"
        if len(set(numbers)) == 1:
            value_eight = "1"
        elif len(set(numbers)) == 2 :
            value_seven = "1"

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        data = {
            'qishu': qishu,
            'number_one': str(result[0]),
            'number_two': str(result[1]),
            'number_three': str(result[2]),
            'value_one': '1' if int(result[3]) > 13 else '0',
            'value_two': '1' if int(result[3]) % 2 != 0 else '0',
            'value_three': str(result[3]),
            'value_four': '1' if int(result[3]) > 13 and int(result[3]) % 2 != 0 else (
                "2" if int(result[3]) <= 13 and int(result[3]) % 2 != 0 else "0"),
            'value_five': '1' if int(result[3]) > 13 and int(result[3]) % 2 == 0 else (
                "2" if int(result[3]) <= 13 and int(result[3]) % 2 == 0 else "0"),
            'value_six': '2' if int(result[3]) <= 5 else ("1" if int(result[3]) >= 22 else '0'),
            'value_seven': value_seven,
            'value_eight': value_eight,
            'value_nine': '1' if result[0] > result[2] else ("0" if result[0] < result[2] else "2"),
            'value_ten': '1' if numbers[2] - numbers[0] == 2 and len(set(numbers)) == 3 else '0',
            #'create_time': nowtime,
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'status': '1',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data
    
    #極速3D
    def jisu3d_get_fields(self, periods, winning_numbers):
        # 傳入的數據： 期數'20180705+0001'， 號碼 [1,2,3]
        qishu = periods
        result = winning_numbers

        # [1,2...] -> '1,2...'
        numbers = ','.join(list(map(str, result)))

        # 取整數的字符串時間戳
        nowtime = str(int(time.time()))

        # -------------插入字典-------------
        data = {
            'qishu': qishu,
            'numbers': numbers,
            'value_one': '1' if int(result[2]) > 4 else '0',#個位 1大 0小
            'value_two': '1' if int(result[2]) % 2 != 0 else '0',#個位 1單 0雙
            'value_three': '1' if int(result[1]) > 4 else '0',#十位 1大 0小
            'value_four': '1' if int(result[1]) % 2 != 0 else '0',#十位 1單 0雙
            'value_five': '1' if int(result[0]) > 4 else '0',#百位 1大 0小
            'value_six': '1' if int(result[0]) % 2 != 0 else '0',#百位 1單 0雙
            'status': '1',
            # 'create_by': 'py5',
            'update_time': nowtime,
            'update_by': 'py5',
            'is_delete': '0',
            'is_effect': '0',
        }
        return data

