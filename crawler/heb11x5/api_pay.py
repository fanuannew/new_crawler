from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    ln11x5 = {}
    headers = {
                    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                }
    url = "http://ho.apiplus.net/newly.do?token=td36da2396686d467k&code=heb11x5&rows=1&format=json"
    # payload = {'SiteId':'15'}
    resp = requests.get(url=url,timeout=6)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext['data'][0]['expect'])
        ymd = periods[:8]
        num = periods[-2:]
        ln11x5['periods'] = ymd + '0' + num

        get_nums = jsontext['data'][0]['opencode']
        num_list = get_nums.split(',')
        ln11x5['numbers'] = list(map('{0:0>2}'.format, num_list))

    print(url[:20],ln11x5)
    return ln11x5

if __name__ == "__main__":
    crawler()