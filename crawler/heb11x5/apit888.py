from bs4 import BeautifulSoup
import requests
import re, json, random, time

def crawler(retry=3):
    fc3d_item = {}
    rndnum = str(random.random())
    headers = {
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3',
                    'Accept-Encoding': 'gzip, deflate',
                    'Connection': 'keep-alive',
                    'Host': 'www.tuoche888.com',
                    'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0',
                }
    url = "http://www.tuoche888.com/api/heb11x5/getLotteryResult?r=" + rndnum
    # print(url)
    resp = requests.get(url=url,headers=headers,timeout=3)

    jsontext = json.loads(resp.text)
    errorCode = jsontext['errorCode']
    if errorCode == 1:
        periods = str(jsontext['data']['current']['period'])
        # ymd = "20" + periods[:6]
        # num = periods[-2:]
        fc3d_item['periods'] = str(periods)

        get_nums = jsontext['data']['current']['award']
        num_list = [str(x) for x in get_nums.split(',')]
        fc3d_item['numbers'] = list(map(str, num_list))

    # 資料驗證，驗證資料長度
    if len(fc3d_item['numbers'])!= 5 or len(fc3d_item['periods']) != 11:
        if retry > 0:
            time.sleep(1.2)
            fc3d_item = crawler(retry=retry-1)
        else:
            fc3d_item = None
    for n in fc3d_item['numbers']:
        if len(n) != 2:
            fc3d_item = None

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()