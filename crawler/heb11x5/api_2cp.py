from bs4 import BeautifulSoup
import requests
import re, json, random

def crawler():
    hlj11x5 = {}
    rndnum = str(random.random())
    headers = {
                    'Accept': 'application/json, text/javascript, */*; q=0.01',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3',
                    'Host': 'www.2cp.com',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0',
                }
    url = "https://www.2cp.com/lottery/heb11x5/getawarddata?t=" + rndnum
    resp = requests.get(url=url,headers=headers,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext['period'])
        ymd = periods[:8]
        num = periods[-2:]
        hlj11x5['periods'] = periods

        get_nums = jsontext['result']
        num_list = get_nums.split(',')
        hlj11x5['numbers'] = list(map('{0:0>2}'.format, num_list))

    print(url[:20],hlj11x5)
    return hlj11x5

if __name__ == "__main__":
    crawler()