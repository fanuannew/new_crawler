from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    heb11x5 = {}
    url = "http://www.hg666b.com/mobile/controller/lottery/getLastResult.php"
    payload = {'gameType':'heb11x5', 'czType':'11x5'}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext['qihao'])
        ymd = periods[:8]
        num = periods[-2:]
        heb11x5['periods'] = ymd + '0' + num

        get_nums = jsontext['result']
        num_list = get_nums.split(',')
        heb11x5['numbers'] = list(map('{0:0>2}'.format, num_list))

    print(url[:20],heb11x5)
    return heb11x5

if __name__ == "__main__":
    crawler()