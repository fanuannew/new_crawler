from bs4 import BeautifulSoup
import requests
import time
import re, sys

def crawler():
    sys.setrecursionlimit(2000)
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Connection': 'keep-alive',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                    'Chrome/71.0.3578.80 Safari/537.36'
                }
        
    url = "http://www.tuoche888.com/heb11x5/history"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    periods_class = soup.select('div.historyPage tr td:nth-of-type(1)')
    number_class = soup.select_one('div.historyPage tr td div').select('span')
    # print('findex::',findex)
    open_nums = []
    for n in number_class:
        open_nums.append(n.text)
    
    # 回傳格式
    heb11x5 = {}
    heb11x5['periods'] = periods_class[0].text
    heb11x5['numbers'] = open_nums
    print(url[:20],heb11x5)
    return heb11x5

if __name__ == "__main__":
    crawler()


    
