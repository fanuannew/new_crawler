## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re

def crawler():
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
                }

    url = "https://www.2cp.com/high/ln11x5"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    number_class = soup.find_all(attrs={"class": "ball24-red"})
    per = soup.find_all(string=re.compile("\d{11}期"))

    p = per[0]

    per_ls = []
    for k in p:
        if k not in ["第","：","期"]:
            per_ls += k
    periods ="".join(per_ls) 

    num_ls = []

    for i in number_class:
        i = i.text
        num_ls += [i]
    number = num_ls[:5]
    # number = ",".join(num)
    # print(periods)
    # print(number)
     # 回傳格式
    ln11x5 = {}
    ln11x5['periods'] = periods
    ln11x5['numbers'] = number
    print(url[:20],ln11x5)
    return ln11x5

if __name__ == "__main__":
    crawler()