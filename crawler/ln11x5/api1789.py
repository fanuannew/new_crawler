from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    ln11x5 = {}
    url = "http://www.1789002.com/game/openLottery?gamekeyvalue=LN11X5&stage=undefined&statu=0&jsonpcallback=jQuery"
    resp = requests.get(url=url,timeout=10)
    jsonstr = resp.text
    start = resp.text.find('(')
    end = resp.text.find(')')
    # 刪除dict以外多餘的東西
    jsonstr = jsonstr[start+1:end]
    # print(jsonstr)
    jsontext = json.loads(jsonstr)
    
    if len(str(jsonstr))> 20:
        periods = str(jsontext['Prev_Stage'])
        ymd = "20" + periods[:6]
        num = periods[-2:]
        ln11x5['periods'] = ymd + '0' + num

        ln11x5['numbers'] = ['{0:0>2}'.format(str(x)) for x in jsontext['Result']]

    print(url[:20],ln11x5)
    return ln11x5

if __name__ == "__main__":
    crawler()