from bs4 import BeautifulSoup
import requests
import re, time

def crawler():
    ln11x5 = {}
    headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-encoding': 'gzip, deflate',
                'Connection': 'keep-alive',
                'Host': '678amz.com',
                'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
            }
    url = "http://678amz.com/ReSyxw/LN11X5/Syxw_index.html"
    s = requests.Session()
    # time.sleep(0.8)
    html = s.get(url=url,headers=headers,timeout=15).content
    soup = BeautifulSoup(html, "html.parser")
    # print(soup)
    period_class = soup.select('div table tr td')
    periods = []
    for elem in period_class:
        # print(elem)
        if re.search(r"\d{8,10}",elem.text):
            periods.append(elem.text)
    # print("有比對出來::",periods)
    periods = periods[0]
    ymd = periods[:6]
    num = periods[-2:]
    ln11x5['periods'] = '20' + ymd + '0' + num

    get_nums = soup.select('div table tr td ul li')
    open_nums = [] #list(map(text, get_nums[:5]))
    for n in get_nums[:5]:
        open_nums.append(n.text.strip())
    # print('開獎號碼::',open_nums)
    ln11x5['numbers'] = list(map('{0:0>2}'.format,open_nums))
    print(url[:20],ln11x5)
    return ln11x5

if __name__ == "__main__":
    crawler()