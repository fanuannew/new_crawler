from bs4 import BeautifulSoup
import requests
import re

def crawler():
    ln11x5 = {}
    headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
            }
    url = "https://m.cjcp.com.cn/kaijiang/ln11x5/"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    period_class = soup.select_one('div.fc_resulte div h1 em')
    number_class = soup.select('div.fc_resulte div p span')
    operiods = re.search(r'\d{10}',period_class.text).group(0)
    ln11x5['periods'] = operiods[:8] +'0'+ operiods[-2:]

    r_nums = []
    for elem in number_class[:5]:
        r_nums.append(elem.text)

    ln11x5['numbers'] = r_nums
    print(url[:20],ln11x5)
    return ln11x5

if __name__ == "__main__":
    crawler()