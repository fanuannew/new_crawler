from bs4 import BeautifulSoup
import requests
import re, json, random

def crawler():
    ln11x5 = {}
    rndnum = str(random.random())
    headers = {
                    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                }
    url = "https://m.2cp.com/api/ln11x5/lottery?r=" + rndnum
    # payload = {'SiteId':'15'}
    resp = requests.get(url=url,timeout=6)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext['period'])
        ymd = periods[:8]
        num = periods[-2:]
        ln11x5['periods'] = periods

        get_nums = jsontext['result']
        num_list = get_nums.split(',')
        ln11x5['numbers'] = list(map('{0:0>2}'.format, num_list))

    print(url[:20],ln11x5)
    return ln11x5

if __name__ == "__main__":
    crawler()