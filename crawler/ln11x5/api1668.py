from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    ln11x5 = {}
    url = "https://www.cp1668.com/ElevenFive/getElevenFiveInfo.do?issue=&lotCode=ln11x5"
    resp = requests.get(url=url,timeout=10)
    jsontext = resp.json()
    errorCode = jsontext['errorCode']
    if errorCode == 0:
        periods = str(jsontext['result']['data']['preDrawIssue'])
        ymd = periods[:8]
        num = periods[-2:]
        ln11x5['periods'] = ymd + '0' + num

        get_nums = jsontext['result']['data']['preDrawCode']
        num_list = [str(x) for x in get_nums.split(',')]
        ln11x5['numbers'] = list(map('{0:0>2}'.format, num_list))

    print(url[:20],ln11x5)
    return ln11x5

if __name__ == "__main__":
    crawler()