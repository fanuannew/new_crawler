from collections import Counter
import re
import sqlite3


def find_most_period(peroids):
    pass


def find_most_comm(ready_key, results, con='temp_open_nums'):
    # 找出指定期數(在此是最多相同結果)的開獎號碼
    # 並實作retry機制, 新的retry機制需加入上一期比對
    new_result = []
    get_value = 0
    for elem in results:
        if elem != None and elem['periods'] == ready_key:
            new_result.append(str(elem['numbers']))
    # print('最多的開獎結果:', Counter(new_result))
    send_numbs = ''
    to_retry = 0
    ## 如果找到的目標來源(符合期數)少於兩個就重試
    if len(new_result) < 2:
        to_retry = 1
    for key, value in Counter(new_result).most_common(1):
        send_numbs = key
        get_value = value
        print('目標開獎結果::',ready_key, ': ',key,'正確來源數::',value)
        if value < 2:
            to_retry = 1
    # print("開獎字串::", send_numbs)
    ## 如果等於1筆就做下面處理
    if len(new_result) == 1:
        # 檢查資料庫有無資料，沒有就做寫入
        try:
            exist_d = db_buffer_read(ready_key, con)
            if len(exist_d) == 0:
                db_buffer_write(ready_key, send_numbs, con)
            elif len(exist_d) == 1:
                to_retry = 0
                send_numbs = new_result[0]
        except Exception as e:
            print('讀取寫入SQLite出錯:', str(e))
        # 資料庫有資料，就決定送出開獎結果
    ## =====================

    ## 把String轉換回List
    junkers = re.compile(r"[\[' \]]")
    real_list = junkers.sub('', send_numbs).split(",")
    ## ==轉換End=========
    # 到此來源比較結束==================================
    # 回傳real_list, value, to_retry
    re_obj = {}
    re_obj['real_list'] = real_list
    # re_obj['value'] = get_value
    re_obj['to_retry'] = to_retry
    return re_obj
    


def return_most_result(results):
    pass

# 寫入資料庫
def db_buffer_write(ready_key, send_numbs, tablename='temp_open_nums'):
    #新的格式新增url || module_name, 加上舊的period跟number
    conn = sqlite3.connect('open_nums.db')
    c = conn.cursor()
    if True: #sqlite_tools.checkTablesExist(tablename):
        # c.execute('create table if not exists '+ tablename +' (id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, period TEXT, win_nums TEXT)')
        values = []
        values.append(ready_key)
        values.append(send_numbs)
        c.execute('insert into '+ tablename +'(period, win_nums) values (?, ?)', values)
    conn.commit()
    conn.close()

# 讀取資料庫
def db_buffer_read(ready_key, tablename='temp_open_nums'):
    new_results = []
    conn = sqlite3.connect('open_nums.db')
    c = conn.cursor()
    c.execute('create table if not exists '+ tablename +' (id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, period TEXT, win_nums TEXT)')
    cursor = conn.execute("SELECT period, win_nums from "+ str(tablename) +" where period=" + str(ready_key) + ' limit 1')
    for row in cursor:
        result = {}
        result['period'] = row[0]
        result['numbers'] = row[1]
        new_results.append(result)
        print('檢測資料庫開獎:',new_results)
        conn.execute('DELETE from '+ str(tablename) +' where period<' + str(ready_key))
        conn.execute('DELETE from '+ str(tablename) +' where period=' + str(ready_key))
        # 重設資料庫ID數字
        conn.execute('delete from sqlite_sequence where name="'+ str(tablename) +'"')
    
    conn.commit()
    conn.close()
    return new_results