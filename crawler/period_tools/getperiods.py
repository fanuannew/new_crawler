import requests
from bs4 import BeautifulSoup
import json, random
from ..config import config_url
from .apilottery import api_name
from ..db_tools import dbquery
dbq = dbquery.Dbquery()

def getperid(con, retry_t):
    # 尋找config檔裡特定的key值
    new_configurl = {}
    for key, val in config_url.items():
        # print("keykey::",key)
        if key.find('official') >=0:
            new_configurl.update({key:val})
    # 隨機抓取一組正式機的url來抓api
    randkey = random.choice(list(config_url.keys()))
    domain = config_url[randkey]
    # 組合指定彩種的api
    api = api_name[con] + "?getOpenNumber"
    url = domain + api
    try:
        resp = requests.get(url=url,timeout=10)
        data = resp.json()
        result = data['data']['last_qishu']
        print(data['data']['last_qishu'])
    except Exception as e:
        print('API取得期數異常:::', str(e))
        if retry_t == 0:
            result= '0'
        else:
            result = getperid(con,(retry_t-1))
    
    return result

def readytocrawl(con):
    open_t = True
    try:
        period = getperid(con, 5)
        # print(period)
        if len(dbq.check_sql_updated(con, period)) > 0:
            print("有開獎資料無須更新")
            open_t = False
        else :
            print('無開獎資料::')
            open_t = True
    except Exception as e:
        print('檢查開獎時間異常::',str(e))
        open_t = True
    return open_t, period
    
    
    

if __name__ == "__main__":
    # getperids("xj11x5")
    readytocrawl("xj11x5")