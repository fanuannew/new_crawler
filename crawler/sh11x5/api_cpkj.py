from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    fc3d_item = {}
    url = "https://www.95cpkj.com/ElevenFive/getElevenFiveInfo.do?issue=&lotCode=sh11x5"
    # payload = {'category':'sh11x5'}
    resp = requests.get(url=url,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    errorCode = jsontext['errorCode']
    if errorCode == 0:
        periods = str(jsontext['result']['data']['preDrawIssue'])
        ymd = periods[:8]
        num = periods[-2:]
        fc3d_item['periods'] = ymd + '0' + num

        get_nums = str(jsontext['result']['data']['preDrawCode']).split(',')
        fc3d_item['numbers'] = list(map('{0:0>2}'.format, get_nums))

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()