from bs4 import BeautifulSoup
import requests
import re, json, time

def crawler(retry=3):
    fc3d_item = {}
    millis = int(round(time.time() * 1000))
    headers = {
                    'Accept': '*/*',
                    'Accept-Language': 'zh-TW,zh;q=0.8,en-US;q=0.5,en;q=0.3',
                    'Accept-Encoding': 'gzip, deflate',
                    'Connection': 'keep-alive',
                    'Host': 'www.cp8200.com',
                    'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0',
                }
    url = "https://www.cp8200.com/ElevenFive/getElevenFiveInfo.do?issue=&lotCode=sh11x5"
    # print(url)
    resp = requests.get(url=url,headers=headers,timeout=3)

    jsontext = json.loads(resp.text)
    errorCode = jsontext['errorCode']
    if errorCode == 0:
        periods = str(jsontext['result']['data']['preDrawIssue'])
        ymd = periods[:8]
        num = periods[-2:]
        fc3d_item['periods'] = ymd + '0' + num

        get_nums = jsontext['result']['data']['preDrawCode']
        num_list = [str(x) for x in get_nums.split(',')]
        fc3d_item['numbers'] = list(map('{0:0>2}'.format, num_list))

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()