from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    heb11x5 = {}
    url = "http://cp568.com/lotteryV3/lotterys.do"
    payload = {'lotCode':'SH11X5', 'needLast':'true'}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext['SH11X5']['lastQiHao'])
        ymd = periods[:8]
        num = periods[-2:]
        heb11x5['periods'] = ymd + '0' + num

        get_nums = jsontext['SH11X5']['lastHaoMa']
        num_list = get_nums.split(',')
        heb11x5['numbers'] = list(map('{0:0>2}'.format, num_list))
    
    # 測試開獎結果能不能轉成數字
    num_test = int(heb11x5['numbers'][0])
    num_test5 = int(heb11x5['numbers'][4])

    print(url[:20],heb11x5)
    return heb11x5

if __name__ == "__main__":
    crawler()