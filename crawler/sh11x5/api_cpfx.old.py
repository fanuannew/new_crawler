from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    fc3d_item = {}
    url = "http://api.cpfx18.com/lottery/lottery/newest?category=sh11x5"
    payload = {'category':'sh11x5'}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    errorCode = jsontext['code']
    if errorCode == 1:
        periods = str(jsontext['data']['issue'])
        ymd = periods[:8]
        num = periods[-2:]
        fc3d_item['periods'] = ymd + '0' + num

        get_nums = str(jsontext['data']['number']).split(',')
        fc3d_item['numbers'] = list(map('{0:0>2}'.format, get_nums))

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()