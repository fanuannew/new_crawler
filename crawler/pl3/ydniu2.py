from bs4 import BeautifulSoup
import requests
import time
import re, sys

def crawler():
    sys.setrecursionlimit(2000)
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Connection': 'keep-alive',
                    'Host': 'chart.ydniu.com',
                    'Referer': 'https://www.google.com/',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                    'Chrome/71.0.3578.80 Safari/537.36'
                }
        
    url = "https://chart.ydniu.com/trend/pl3/"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    # print(soup)
    periods_class = soup.select('tr td')
    # number_class_45 = soup.find_all(attrs={"class": "flanse"})
    findex = [i for i, item in enumerate(periods_class) if re.search(r"20\d{5}",item.text)]
    # print('findex::',findex)
    row = findex[-1]
    open_nums = []
    period = periods_class[row].text
    periods = period
    for n in range(1,4):
        open_nums.append(periods_class[row+n].text)
    
    # 回傳格式
    pl3_item = {}
    pl3_item['periods'] = periods
    pl3_item['numbers'] = open_nums
    print(url[:20],pl3_item)
    return pl3_item

if __name__ == "__main__":
    crawler()


    
