from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    pl3_item = {}
    headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-encoding': 'gzip, deflate',
                'Connection': 'keep-alive',
                'Host': 'www.1395p.com',
                'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
            }
    url = "https://www.1395p.com/pl3/getawarddata"
    resp = requests.get(url=url,headers=headers,timeout=10)
    jsontext = resp.json()
    # print(jsontext)
    if len(str(jsontext)) > 20:
        date = str(jsontext['current']['awardTime'])
        year = date[0:4]
        month = date[5:7]
        day = date[8:10]
        periods = str(jsontext['current']['period'])
        pl3_item['periods'] = periods

        get_nums = jsontext['current']['result']
        num_list = [str(x) for x in get_nums.split(',')]
        pl3_item['numbers'] = list(map(str, num_list))

    print(url[:20],pl3_item)
    return pl3_item

if __name__ == "__main__":
    crawler()