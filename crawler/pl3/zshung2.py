from bs4 import BeautifulSoup
import requests
import re

def crawler():
    pl3_item = {}
    headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Host': 'www.itxyg.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
            }
    url = "http://www.senweibz.com/pl3/history"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    period_class = soup.select('div.awardContent ul li span')
    number_class = soup.select('div.awardContent ul li span span')
    # print(period_class,number_class)
    operiods = re.findall(r'\d{7}',str(period_class))
    pl3_item['periods'] = operiods[0]

    r_nums = []
    for elem in number_class[:3]:
        r_nums.append(elem.text)

    pl3_item['numbers'] = r_nums
    print(url[:20],pl3_item)
    return pl3_item

if __name__ == "__main__":
    crawler()