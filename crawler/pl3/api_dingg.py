from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    pl3_item = {}
    url = "https://dingg1.com/queryHisResult.php"
    payload = {'gameType':'PL3','startRow':'1','endRow':'10'}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    parstr = jsontext[0]
    # print(parstr)
    if len(parstr) > 1:
        periods = str(parstr['returnList'][0]['issueId'])
        ymd = "20"
        num = periods
        pl3_item['periods'] = ymd + num

        get_nums = parstr['returnList'][0]['resultsStr']
        num_list = get_nums.split(',')
        pl3_item['numbers'] = list(map(str, num_list))

    print(url[:20],pl3_item)
    return pl3_item

if __name__ == "__main__":
    crawler()