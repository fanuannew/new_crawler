from bs4 import BeautifulSoup
import requests
import re

def crawler():
    pl3_item = {}
    headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
            }
    url = "https://m.cjcp.com.cn/kaijiang/pl3/"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    period_class = soup.select_one('div.fc_resulte div h1 em')
    number_class = soup.select('div.fc_resulte div p span')
    operiods = re.search(r'\d{7}',period_class.text).group(0)
    pl3_item['periods'] = operiods

    r_nums = []
    for elem in number_class[:3]:
        r_nums.append(elem.text)

    pl3_item['numbers'] = r_nums
    print(url[:20],pl3_item)
    return pl3_item

if __name__ == "__main__":
    crawler()