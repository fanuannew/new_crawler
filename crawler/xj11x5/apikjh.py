from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    xj11x5 = {}
    headers = {
                    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                }
    url = "https://kjh.55128.cn/k/Template/_getKjData"
    payload = {'lotterytype':'GP_11x5_XinJiang'}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext['Term'])
        ymd = periods[:6]
        num = periods[-2:]
        xj11x5['periods'] = '20'+ ymd + '0' + num

        get_nums = jsontext['RedBall']
        num_list = []
        for elem in get_nums:
            num_list.append(str(elem))
        xj11x5['numbers'] = list(map('{0:0>2}'.format, num_list))

    print(url[:20],xj11x5)
    return xj11x5

if __name__ == "__main__":
    crawler()