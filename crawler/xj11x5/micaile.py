## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re

def crawler():
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
                }
    xj11x5 = {}
    url = "https://m.pub.icaile.com/xj11x5/"
    
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    periods = soup.select('table tbody tr td:nth-of-type(1)')

    # print('peroids:::', periods)
    # 開獎號碼[1,2,3,4,5]
    get_nums = soup.select('table tbody tr td em')
    if len(get_nums) % 5 ==0 :
        # 可以被5整除才繼續進行之後的取值處理
        row = len(get_nums) // 5
        # print('可以整除:::',row,' 期數:::', periods[row-1])
        period = periods[row-1].text
        ymd = period[:6]
        num = period[-2:]
        xj11x5['periods'] = '20' + ymd + '0' + num
        get_nums = get_nums[-5:]
        open_nums = []
        for n in get_nums:
            open_nums.append(n.text)
        # print('open_nums:::',open_nums)
        xj11x5['numbers'] = list(map('{0:0>2}'.format, open_nums))

    # print(periods)
    # print(number)
    print(url[:20],xj11x5)
    return xj11x5

if __name__ == "__main__":
    crawler()