## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re

def crawler():
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'Host': 'gd11x5.icaile.com',
                    'Cookie': '_ga=GA1.2.1011067338.1545037475; UM_distinctid=167bb68ecda757-0fb76f22ddc227-b78173e-1fa400-167bb68ecdb2dc; __cfduid=d208e61e6e51a10d69b0d347bdaaf66b51545038910; _gid=GA1.2.38146185.1548912330; Hm_lvt_9e8b7756afa837e72f9f38fb6ca5c3bf=1546396077,1548912330,1548915064,1548915072; Hm_lpvt_9e8b7756afa837e72f9f38fb6ca5c3bf=1548920199; PHPSESSID=i8l9im7qg769ihl92c1gh6gqk0',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
                }
    xj11x5 = {}
    url = "http://xj11x5.icaile.com/"
    
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    print(soup.text)
    periods = soup.select('table tbody tr td:nth-of-type(1)')

    # print('peroids:::', periods)
    # 開獎號碼[1,2,3,4,5]
    get_nums = soup.select('table tbody tr td em')
    if len(get_nums) % 5 ==0 :
        # 可以被5整除才繼續進行之後的取值處理
        row = len(get_nums) // 5
        # print('可以整除:::',row,' 期數:::', periods[row-1])
        period = periods[row-1].text
        ymd = period[:6]
        num = period[-2:]
        xj11x5['periods'] = '20' + ymd + '0' + num
        get_nums = get_nums[-5:]
        open_nums = []
        for n in get_nums:
            open_nums.append(n.text)
        # print('open_nums:::',open_nums)
        xj11x5['numbers'] = list(map('{0:0>2}'.format, open_nums))

    # print(periods)
    # print(number)
    print(url[:20],xj11x5)
    return xj11x5

if __name__ == "__main__":
    crawler()