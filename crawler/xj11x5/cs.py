## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re

def crawler():
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
            }

    url = "http://822cs.com/XJ11x5/List.Aspx?Cid=null&v=1"
    html = requests.get(url=url,headers=headers,timeout=5).content
    soup = BeautifulSoup(html, "html.parser")
    number_str = soup.find_all(string=re.compile("\(\d{2}-\d{2}-\d{2}-\d{2}-\d{2},总和\d{2}\)"))
    per = soup.find_all(string=re.compile("【\d{4}年\d{2}月\d{2}日】"))
    per_n = soup.find_all(string=re.compile("第\d{3}期"))


    n = number_str[0]

    num_ls = []
    for s in n:
        if s not in ["(","总","和",")"]:
            num_ls += [s]
    num = "".join(num_ls)
    number = num.replace("-",",").split(",")
    number = number[:5]


    pn = per_n[0]
    a = [ps for ps in pn if ps not in ["第","期"]]
    b = "".join(a)


    pers = []
    p = per[0]
    for ps in p:
        if ps not in ["【","年","月","日","】"]:
            pers += [ps]

    # pers.insert(6, "0")
    periods = "".join(pers)
    periods = periods + b

    # print(periods)
    # print(number)
    # 回傳格式
    xj11x5 = {}
    xj11x5['periods'] = periods
    xj11x5['numbers'] = number
    print(url[:20],xj11x5)
    return xj11x5

if __name__ == "__main__":
    crawler()