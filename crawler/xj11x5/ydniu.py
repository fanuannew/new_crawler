from bs4 import BeautifulSoup
import requests
import time
import re, sys

def crawler():
    sys.setrecursionlimit(2000)
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Connection': 'keep-alive',
                    'Host': 'chart.ydniu.com',
                    'Referer': 'https://www.google.com/',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                    'Chrome/71.0.3578.80 Safari/537.36'
                }
        
    url = "https://chart.ydniu.com/trend/syx5xj/"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    number_class_3 = soup.find_all(attrs={"class": "hong"})
    number_class_45 = soup.find_all(attrs={"class": "flanse"})
    per = soup.find_all(string=re.compile("20\d{8}"))

    number_ls3 = []
    for i in number_class_3:
        i = i.text    
        number_ls3 += [i]

    limit = 3
    a = [number_ls3[i:i + limit] for i in range(0, len(number_ls3), limit)]
    num3 = a[-1]

    number_ls45 = []
    for j in number_class_45:
        j = j.text
        number_ls45 += [j]

    limit = 2
    b = [number_ls45[i:i + limit] for i in range(0, len(number_ls45), limit)]    
    num45 = b[-1]

    num_ls = num3 + num45

    # number = ",".join(num_ls)
    number = num_ls

    periods = per[-1:]

    # with open("./log/ydniu_periods.txt", "w") as f:
    #     f.write(periods)

    # with open("./log/ydniu_number.txt", "w") as f:
    #     f.write(number)
    # 回傳格式
    xj11x5 = {}
    xj11x5['periods'] = periods
    xj11x5['numbers'] = number
    return xj11x5

if __name__ == "__main__":
    crawler()


    
