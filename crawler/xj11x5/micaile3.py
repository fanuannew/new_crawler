## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re

def crawler():
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate',
                    'Connection': 'keep-alive',
                    'Host': 'm.xj11x5.icaile.com',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
                }
    xj11x5 = {}
    url = "http://m.xj11x5.icaile.com/kaijiang/jt"
    
    html = try_get_html(url=url,headers=headers,try_t=4)
    soup = BeautifulSoup(html, "html.parser")
    pers = soup.select('table tr td')
    finder = re.compile('\d{6}-\d{2}')
    periods = finder.findall(str(pers))

    # print('peroids:::', periods)
    # 開獎號碼[1,2,3,4,5]
    get_nums = soup.select('table tr td span')
    # print('get_nums:::', get_nums)
    if len(get_nums) % 5 ==0 :
        # 可以被5整除才繼續進行之後的取值處理
        row = len(get_nums) // 5
        # print('可以整除:::',row,' 期數:::', periods[-1:][0])
        period = periods[row-1]
        ymd = period[:6]
        num = period[-2:]
        xj11x5['periods'] = '20' + ymd + '0' + num
        get_nums = get_nums[-5:]
        open_nums = []
        for n in get_nums:
            open_nums.append(n.text)
        # print('open_nums:::',open_nums)
        xj11x5['numbers'] = list(map('{0:0>2}'.format, open_nums))

    # 第二種解法(預防萬一比對問題)
    get_result = soup.select('table tr')
    for n in get_result[-3:]:
        if n.select_one('td') != None and re.search(r"\d{6,8}",n.select_one('td').text):
            new_per = n.select_one('td').text
            new_nums = n.select('td span')
    open_nums = []
    for n in get_nums:
            open_nums.append(n.text)

    if  xj11x5['numbers'] == list(map('{0:0>2}'.format, open_nums)):
        xj11x5['periods'] = '20' + new_per[:6] + '0' + new_per[-2:]
    else: 
        xj11x5['periods'] = '0'
    
    print(url[:20],xj11x5)
    return xj11x5

def try_get_html(url, headers, try_t):
    try_nt = try_t-1
    if try_t > 0:
        try:
            result = requests.get(url=url,headers=headers,timeout=3).content
        except Exception as e:
            result = try_get_html(url, headers, try_nt)
    return result

if __name__ == "__main__":
    crawler()