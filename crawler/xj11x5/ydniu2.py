from bs4 import BeautifulSoup
import requests
import time
import re, sys

def crawler():
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Connection': 'keep-alive',
                    'Host': 'chart.ydniu.com',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'+
                    'Chrome/71.0.3578.80 Safari/537.36'
                }
        
    url = "https://chart.ydniu.com/trend/syx5xj/"
    payload = {'methond': 'CheckUpdate','index':'0'}
    html = requests.post(url=url,headers=headers,data=payload,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    periods_class = soup.select('div table tbody tr td')
    # number_class_45 = soup.find_all(attrs={"class": "flanse"})
    findex = [i for i, item in enumerate(periods_class) if re.search(r"20\d{8}",item.text)]
    # print('findex::',findex)
    row = findex[-1]
    open_nums = []
    period = periods_class[row].text
    periods = period[:8]+'0'+period[-2:]
    for n in range(1,6):
        open_nums.append(periods_class[row+n].text)

    # print(periods,":::",open_nums)
    # 回傳格式
    xj11x5 = {}
    xj11x5['periods'] = periods
    xj11x5['numbers'] = open_nums
    print(url[:20],xj11x5)
    return xj11x5

if __name__ == "__main__":
    crawler()


    
