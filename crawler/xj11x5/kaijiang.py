from selenium.webdriver.firefox.options import Options
from selenium import webdriver
from bs4 import BeautifulSoup
import re

# Firefoxy headless mode
opts = Options()
opts.set_headless(headless=True)
assert opts.headless

def crawler():
    try:
        path = r'C:\Users\TL_User\Desktop\test\geckodriver.exe' # win
                # path = "/usr/bin/geckodriver" # linux
        driver = webdriver.Firefox(options=opts,executable_path = path)

        url = "https://kaijiang.500.com/xjsyxw.shtml"
        driver.get(url)
        driver.implicitly_wait(5) # 隱性等待5，超過跳出error 
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        periods_td = soup.find_all(string=re.compile("\d{6}-\d{2}"))
        number_td = soup.find_all(string=re.compile("\d{2},\d{2},\d{2},\d{2},\d{2}"))
        a = periods_td[0].replace("-","0")
        periods = "20"+ a
        number = number_td[0]
        periods = str(periods)
        number = str(number)
        driver.quit()

        with open("./log/kaijiang_periods.txt", "w") as f:
            f.write(periods)

        with open("./log/kaijiang_number.txt", "w") as f:
            f.write(number)
        # 回傳格式
        xj11x5 = {}
        xj11x5['periods'] = periods
        xj11x5['numbers'] = number
        return xj11x5
        
    except:
        pass
    
if __name__ == "__main__":
    crawler()