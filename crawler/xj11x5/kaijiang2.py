from bs4 import BeautifulSoup
import requests
import re

def crawler():
    headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
            }
    url = "https://m.500.com/info/kaijiang/xjsyxw/"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    period_class = soup.select('div ul li:nth-of-type(1)')
    number_class = soup.select('div ul li:nth-of-type(3)')
    operiods = period_class[len(period_class)-1].text
    periods = '20' + operiods[:6] +'0'+ operiods[-2:]

    number_str = number_class[len(number_class)-1].text
    number = number_str.strip(' \t\n\r').split(',')

    # print(int(periods))
    # print(number)
     # 回傳格式
    xj11x5 = {}
    xj11x5['periods'] = periods
    xj11x5['numbers'] = number
    print(url[:20],xj11x5)
    return xj11x5

if __name__ == "__main__":
    crawler()