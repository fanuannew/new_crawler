## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re, time

def crawler():
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
                }
    xj11x5 = {}
    url1 = "https://www.55564400.com/m/lottery/result_ssc.php?type_str=xj11x5"
    url2 = 'https://www.55564400.com/m/lottery/result_ssc.php?type_str=xj11x5&t=2'
    nowhour = time.localtime().tm_hour
    if nowhour <=3 and nowhour>=0:
        url = url2
    else:
        url = url1
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    number_class = soup.select_one('li:nth-of-type(1) div span').text

    xj11x5['periods'] = number_class[-11:]
    print('peroids', xj11x5['periods'])
    # 開獎號碼[1,2,3,4,5]
    open_nums = []
    get_nums = soup.select('li:nth-of-type(1) div img')
    traper = re.compile('/')
    for elem in get_nums:
        match = re.search('(\d*).png',elem['src'])
        open_nums.append('{0:0>2}'.format(match.group(1)))
    print('open_nums::',open_nums)
    # print(open_nums)
    xj11x5['numbers'] = open_nums

    # print(periods)
    # print(number)
    return xj11x5

if __name__ == "__main__":
    crawler()