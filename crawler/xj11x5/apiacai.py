from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    xj11x5 = {}
    url = "https://m.acaicp.com/Ajax/10001"
    payload = {'LotIds':111013}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    errorCode = jsontext['Error']
    if errorCode == 'success':
        parstr = json.loads(jsontext['Value'])
        periods = str(parstr['Xj11x5']['OpenDetail']['TermNo'])
        ymd = "20" + periods[:6]
        num = periods[-2:]
        xj11x5['periods'] = ymd + '0' + num

        get_nums = parstr['Xj11x5']['OpenDetail']['ArrayNum']
        xj11x5['numbers'] = list(map(str, get_nums))

    print(url[:20],xj11x5)
    return xj11x5

if __name__ == "__main__":
    crawler()