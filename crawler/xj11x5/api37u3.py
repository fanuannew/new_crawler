from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    xj11x5 = {}
    headers = {
                    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                }
    url = "http://www.bet80088.com/mobile/controller/getLotteryHistory.php"
    payload = {'date': 'date','type':'xj11x5','czType':'11x5'}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext[0]['qishu'])
        ymd = periods[:8]
        num = periods[-2:]
        xj11x5['periods'] = ymd + '0' + num

        get_nums = jsontext[0]['jieguo']
        num_list = get_nums.split(',')
        xj11x5['numbers'] = list(map('{0:0>2}'.format, num_list))

    print(url[:20],xj11x5)
    return xj11x5

if __name__ == "__main__":
    crawler()