from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    bjpc28_item = {}
    headers = {
                'accept-encoding': 'gzip, deflate',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
            }
    url = "http://m.jspc030.com/m/php/action.php?action=lotterycharts"
    payload = {'pcmn': '6008-150-1','game_type':'xy28','count_term':'50','version':'200'}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = resp.json()
    # print(jsontext)
    if len(str(jsontext)) > 20:
        # date = str(jsontext['current']['period'])
        # year = date[0:4]
        # month = date[5:7]
        # day = date[8:10]
        periods = jsontext['data']['list'][-1:][0]
        # print(periods)
        bjpc28_item['periods'] = periods[0]

        get_nums = periods[2]
        num_list = [str(x) for x in get_nums.split(',')]
        bjpc28_item['numbers'] = num_list[:4]

    print(url[:20],bjpc28_item)
    return bjpc28_item

if __name__ == "__main__":
    crawler()