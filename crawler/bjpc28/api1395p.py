from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    bjpc28_item = {}
    headers = {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-encoding': 'gzip, deflate',
                'Connection': 'keep-alive',
                'Host': 'www.1395p.com',
                'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
            }
    url = "https://www.1395p.com/pcdd/getawarddata"
    resp = requests.get(url=url,headers=headers,timeout=10)
    jsontext = resp.json()
    # print(jsontext)
    if len(str(jsontext)) > 20:
        # date = str(jsontext['current']['period'])
        # year = date[0:4]
        # month = date[5:7]
        # day = date[8:10]
        periods = str(jsontext['current']['period'])
        bjpc28_item['periods'] = periods

        get_nums = jsontext['current']['result']
        num_list = [str(x) for x in get_nums.split(',')]
        sum_num = sum(int(i) for i in num_list)
        num_list.append(str(sum_num))
        bjpc28_item['numbers'] = num_list

    print(url[:20],bjpc28_item)
    return bjpc28_item

if __name__ == "__main__":
    crawler()