from bs4 import BeautifulSoup
import requests
import time
import re, sys

def crawler():
    sys.setrecursionlimit(2000)
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Connection': 'keep-alive',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                    'Chrome/71.0.3578.80 Safari/537.36'
                }
        
    url = "https://www.28yuce.com/pc28/"
    html = requests.get(url=url,headers=headers,timeout=6).content
    soup = BeautifulSoup(html, "html.parser")
    periods_class = soup.select('table tr')

    open_data = []
    for n in periods_class:
        pre_data = n.select('td')
        if n.select_one('td')!=None and pre_data[1].text != '':
            open_data = pre_data
            break
    # print(open_data)
    # print(periods_class)
    finder = re.compile('\d{1,2}')
    periods = finder.findall(open_data[2].text)
    # print(periods)
    # 把第四位(總和)去除掉前面的0
    periods[3] = str(int(periods[3]))
    # 回傳格式
    bjpc28_item = {}
    bjpc28_item['periods'] = open_data[0].text
    bjpc28_item['numbers'] = periods[:4]
    print(url[:20],bjpc28_item)
    return bjpc28_item

if __name__ == "__main__":
    crawler()


    
