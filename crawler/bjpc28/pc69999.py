## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re

def crawler():
    bjpc28_item = {}
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
                }

    url = "http://pc69999.com/index.php?m=Home&c=WebPc&a=dt_result"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")

    bjpc28_item['periods'] = soup.select_one('div.tone.fleft span span').text

    classes = soup.select_one('table.dt_table tr td b').text
    # print(classes)
    r_nums = []
    finder = re.compile('\d{1,2}')
    get_nums = finder.findall(classes)
    # 把第四位(總和)除去0
    get_nums[3] = str(int(get_nums[3]))
    # print(get_nums)
    bjpc28_item['numbers'] = get_nums[:4]
    # 回傳格式
    print(url[:20],bjpc28_item)
    return bjpc28_item

if __name__ == "__main__":
    crawler()