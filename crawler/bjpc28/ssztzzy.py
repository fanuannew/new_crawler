from bs4 import BeautifulSoup
import requests
import time
import re, sys

def crawler():
    sys.setrecursionlimit(2000)
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Connection': 'keep-alive',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                    'Chrome/71.0.3578.80 Safari/537.36'
                }
        
    url = "http://0330b8.ssztzzy.com/?c=record&a=game&lottery_type=pcdd"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    periods_class = soup.select_one('div.list .row .col-33 div div:nth-of-type(1)').text
    # print(periods_class)
    number_class = soup.select('div.list .col-67 div:nth-of-type(1)')
    # print(number_class)
    open_nums = []
    finder = re.compile("\d{1}")
    get_nums = finder.findall(number_class[0].text)
    open_nums = get_nums[:3]
    sum_num = sum(int(i) for i in open_nums[:3])
    open_nums.append(str(sum_num))
    
    # 回傳格式
    bjpc28_item = {}
    bjpc28_item['periods'] = periods_class
    bjpc28_item['numbers'] = open_nums
    print(url[:20],bjpc28_item)
    return bjpc28_item

if __name__ == "__main__":
    crawler()


    
