from bs4 import BeautifulSoup
import requests
import re, json, datetime, time

def crawler():
    hkmk6_item = {}
    year = datetime.datetime.today().strftime('%Y')
    millis = int(round(time.time() * 1000))
    url = "http://hnkjw.cc/api/newest?code=xy28&t=" + str(millis)
    # payload = {'year': year,'type':'1'}
    resp = requests.get(url=url,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext['data']['newest']['issue'])
        # ymd = year
        # num = '{0:0>3}'.format(periods)
        hkmk6_item['periods'] = periods

        get_nums = jsontext['data']['newest']['code']
        num_list = [str(x) for x in get_nums.split(',')]
        sum_num = sum(int(i) for i in num_list)
        num_list.append(str(sum_num))
        hkmk6_item['numbers'] = num_list

    print(url[:20],hkmk6_item)
    return hkmk6_item

if __name__ == "__main__":
    crawler()