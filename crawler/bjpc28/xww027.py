from bs4 import BeautifulSoup
import requests
import time
import re, sys

def crawler():
    sys.setrecursionlimit(2000)
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'Connection': 'keep-alive',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                    'Chrome/71.0.3578.80 Safari/537.36'
                }
        
    url = "https://m.95234.org/xy28/numlist?SearchDate=&PageSize=200"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")
    periods_class = soup.select_one('table tr td:nth-of-type(1)').text
    # print(periods_class)
    finder = re.compile('\d{4,6}')
    periods = finder.findall(periods_class)
    # print(periods)
    number_class = soup.select_one('table tr td:nth-of-type(3)').select('i')
    # print(number_class)
    open_nums = []
    for n in number_class[:3]:
        open_nums.append(n.text)
    sum_num = sum(int(i.text) for i in number_class[:3])
    open_nums.append(str(sum_num))
    # 回傳格式
    bjpc28_item = {}
    bjpc28_item['periods'] = periods[0]
    bjpc28_item['numbers'] = open_nums
    print(url[:20],bjpc28_item)
    return bjpc28_item

if __name__ == "__main__":
    crawler()


    
