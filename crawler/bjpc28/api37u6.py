from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    bjpc28_item = {}
    headers = {
                    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                }
    url = "http://376013.com/mobile/controller/getLotteryHistory.php"
    payload = {'date': 'date','type':'pcdd','czType':'k3cz'}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext[0]['qishu'])
        # ymd = periods[:8]
        # num = periods[-2:]
        bjpc28_item['periods'] = periods

        get_nums = jsontext[0]['jieguo']
        num_list = get_nums.split(',')
        sum_num = sum(int(i) for i in num_list)
        num_list.append(str(sum_num))
        bjpc28_item['numbers'] = num_list

    print(url[:20],bjpc28_item)
    return bjpc28_item

if __name__ == "__main__":
    crawler()