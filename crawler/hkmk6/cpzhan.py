## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re

def crawler():
    hkmk6_item = {}
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
                }

    url = "https://www.cpzhan.com/liu-he-cai/all-results"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")

    p_list = soup.select('div.mytable tbody tr')
    r_list= p_list[-1:][0].select('td')
    # print("r_list:::", r_list)
    qishu=""
    num_list = []
    for index,item in enumerate(r_list):
        if index==0 :
            qishu += item.text
        if index == 1:
            qishu += '{0:0>3}'.format(item.text)
        elif index>2:
            num_list.append('{0:0>2}'.format(item.text))
    # print("qishu:::", qishu)
    # print("num_list:::", num_list)
    hkmk6_item['periods'] = qishu
    hkmk6_item['numbers'] = list(map(str, num_list))
     # 回傳格式
    print(url[:20],hkmk6_item)
    return hkmk6_item

if __name__ == "__main__":
    crawler()