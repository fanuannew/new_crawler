from bs4 import BeautifulSoup
import requests
import re, json, datetime

def crawler():
    hkmk6_item = {}
    year = datetime.datetime.today().strftime('%Y')
    url = "http://goodkaijiang.com/6h/smallSix/findSmallSixHistory.do"
    payload = {'year': year,'type':'1'}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = resp.json()
    errorCode = jsontext['errorCode']
    if errorCode == 0:
        periods = str(jsontext['result']['data']['bodyList'][0]['issue'])
        ymd = year
        num = '{0:0>3}'.format(periods)
        hkmk6_item['periods'] = ymd + num

        get_nums = jsontext['result']['data']['bodyList'][0]['preDrawCode']
        num_list = [str(x) for x in get_nums.split(',')]
        hkmk6_item['numbers'] = list(map('{0:0>2}'.format, num_list))

    print(url[:20],hkmk6_item)
    return hkmk6_item

if __name__ == "__main__":
    crawler()