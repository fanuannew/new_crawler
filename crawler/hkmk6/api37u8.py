from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    hkmk6_item = {}
    headers = {
                    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
                }
    url = "https://www.fff365.com/mobile/controller/getLotteryHistory.php"
    payload = {'date': 'date','type':'six','czType':''}
    resp = requests.post(url=url,data=payload,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext[0]['qishu'])
        # ymd = periods[:8]
        # num = periods[-2:]
        hkmk6_item['periods'] = periods

        get_nums = jsontext[0]['jieguo']
        num_list = get_nums.split(',')
        hkmk6_item['numbers'] = list(map('{0:0>2}'.format, num_list))

    print(url[:20],hkmk6_item)
    return hkmk6_item

if __name__ == "__main__":
    crawler()