from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    hkmk6_item = {}
    headers = {
                'accept-encoding': 'gzip, deflate',
                'Host': 'api.kg-8.com',
                'Origin': 'http://m.jspc028.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
            }
    url = "https://api.kg-8.com/Infos/HistoryList?lid=7&pageIndex=1&pageSize=10"
    # payload = {'pcmn': '6008-150-1','game_type':'xy28','count_term':'50','version':'200'}
    resp = requests.get(url=url,timeout=10)
    jsontext = resp.json()
    # print(jsontext)
    if len(str(jsontext)) > 20:
        # date = str(jsontext['current']['period'])
        # year = date[0:4]
        # month = date[5:7]
        # day = date[8:10]
        periods = jsontext['result'][0]['qiShu']
        # print(periods)
        hkmk6_item['periods'] = str(periods)

        get_nums = jsontext['result'][0]['balls']
        num_list = ['{0:0>2}'.format(str(x)) for x in get_nums.split(',')]
        hkmk6_item['numbers'] = num_list

    print(url[:20],hkmk6_item)
    return hkmk6_item

if __name__ == "__main__":
    crawler()