## 新疆 11選 5 ##

from bs4 import BeautifulSoup
import requests
import re

def crawler():
    hkmk6_item = {}
    headers = {
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'accept-encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'
                }

    url = "http://www.cx997.com/index.php?m=Home&c=WebSix&a=detail"
    html = requests.get(url=url,headers=headers,timeout=10).content
    soup = BeautifulSoup(html, "html.parser")

    hkmk6_item['periods'] = soup.select_one('div.tone.fleft span span').text

    classes = soup.select('table.dt_table td div')
    r_nums = []
    for index,item in enumerate(classes):
        if index>6:
            break
        n1 = str(item['class'][1]) 
        s = n1[4:len(n1)]
        r_nums.append('{0:0>2}'.format(s))
    # r_nums = get_re_nums(r_nums)
    hkmk6_item['numbers'] = r_nums
     # 回傳格式
    print(url[:20],hkmk6_item)
    return hkmk6_item

if __name__ == "__main__":
    crawler()