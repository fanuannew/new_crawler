from bs4 import BeautifulSoup
import requests
import re, json, time

def crawler():
    fc3d_item = {}
    millis = int(round(time.time() * 1000))
    url = "http://potransl.com/webapi/base_getawarddata?code=js11x5&r="+ str(millis)
    # payload = {'category':'sh11x5'}
    resp = requests.get(url=url,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    errorCode = jsontext['status']
    if errorCode == 200:
        periods = str(jsontext['data']['current']['period'])
        ymd = periods[:8]
        num = periods[-2:]
        fc3d_item['periods'] = ymd + '0' + num

        get_nums = str(jsontext['data']['current']['result']).split(',')
        fc3d_item['numbers'] = list(map('{0:0>2}'.format, get_nums))

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()