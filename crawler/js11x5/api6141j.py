from bs4 import BeautifulSoup
import requests
import re, json, time

def crawler():
    fc3d_item = {}
    millis = int(round(time.time() * 1000))
    url = "http://6141jh.com/plan/api.do?code=js11x5&plan=0&size=20&planSize=20&_t="+ str(millis)
    # payload = {'category':'sh11x5'}
    resp = requests.get(url=url,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    errorCode = jsontext['code']
    if errorCode == 1:
        periods = str(jsontext['data']['opendata']['expect'])
        ymd = periods[:8]
        num = periods[-2:]
        fc3d_item['periods'] = periods

        get_nums = str(jsontext['data']['opendata']['code']).split(',')
        fc3d_item['numbers'] = list(map('{0:0>2}'.format, get_nums))

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()