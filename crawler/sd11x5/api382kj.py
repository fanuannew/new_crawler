from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    ln11x5 = {}
    url = "https://382kj.cc/api/newest?code=sd11x5"
    resp = requests.get(url=url,timeout=8)
    jsontext = json.loads(resp.text)
    code = jsontext['code']
    if code == 0:
        ln11x5['periods'] = str(jsontext['data']['newest']['issue'])

        get_nums = jsontext['data']['newest']['array']
        ln11x5['numbers'] = list(map('{0:0>2}'.format, get_nums))

    print(url[:20],ln11x5)
    return ln11x5

if __name__ == "__main__":
    crawler()