from bs4 import BeautifulSoup
import requests
import re, json, time

def crawler():
    fc3d_item = {}
    millis = int(round(time.time() * 1000))
    url = "http://466app.com/lottery/trendChart/lotteryOpenNum.do?lotCode=SD11X5&recentDay=1&rows=30&timestamp="+ str(millis)
    # payload = {'category':'sh11x5'}
    resp = requests.post(url=url,timeout=10)
    jsontext = json.loads(resp.text)
    # print(jsontext)
    if len(str(jsontext))>5:
        periods = str(jsontext[0]['qiHao'])
        ymd = periods[:8]
        num = periods[-2:]
        fc3d_item['periods'] = ymd + '0' + num

        get_nums = str(jsontext[0]['haoMa']).split(',')
        fc3d_item['numbers'] = list(map('{0:0>2}'.format, get_nums))

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()