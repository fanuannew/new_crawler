import time, re, random
import multiprocessing
from bs4 import BeautifulSoup as bs
from collections import Counter
from . import api382kj, api1395p, api1888,apiacai,apipp359,cp2,ydniu2, apiicaile, api_pay, api466app, api1393c, micaile3
import sys
from ..db_tools import dbquery
from ..period_tools.getperiods import getperid as gtpd
from ..period_tools.getperiods import readytocrawl
from ..period_tools.mqtt_alarm import alarm_messages
from ..period_tools.comp_pers import find_most_comm
from ..period_tools.check_prize import check_sql_prizedata
db = dbquery.Dbquery()

def spyder_1():
    try:
        return api382kj.crawler()
    except Exception as e:
        print('1異常:::', str(e))
def spyder_2(): 
    try:
        return api1395p.crawler()
    except Exception as e:
        print('2異常:::', str(e))
def spyder_3(): 
    try:
        return api1888.crawler()
    except Exception as e:
        print('3異常:::', str(e))
def spyder_4(): 
    try:
        return apiacai.crawler()
    except Exception as e:
        print('4異常:::', str(e))
def spyder_5(): 
    try:
        return apipp359.crawler()
    except Exception as e:
        print('5異常:::', str(e))
def spyder_6(): 
    try:
        return cp2.crawler()
    except Exception as e:
        print('6異常:::', str(e))
def spyder_7(): 
    try:
        return ydniu2.crawler()
    except Exception as e:
        print('7異常:::', str(e))
def spyder_8(): 
    try:
        return api1393c.crawler()
    except Exception as e:
        print('8異常:::', str(e))
def spyder_9(): 
    try:
        return api_pay.crawler()
    except Exception as e:
        print('9異常:::', str(e))
def spyder_10(): 
    try:
        return api466app.crawler()
    except Exception as e:
        print('10異常:::', str(e))
def spyder_11(): 
    try:
        return micaile3.crawler()
    except Exception as e:
        print('11異常:::', str(e))

def main(retry):
    sys.setrecursionlimit(5000)
    sd11x5_item = {}
    aresults = []
    results = []
    mapping = {
        "spyder_1": spyder_1,
        "spyder_2": spyder_2,
        "spyder_3": spyder_3,
        "spyder_4": spyder_4,
        "spyder_5": spyder_5,
        "spyder_6": spyder_6,
        "spyder_7": spyder_7,
        "spyder_8": spyder_8,
        "spyder_9": spyder_9,
        "spyder_10": spyder_10,
        "spyder_11": spyder_11
    }
    # 定義爬蟲的來源數量
    spider_num = 11
    # 加入資料庫判斷要不要進行爬蟲
    # 定義要不要依賴API加速比對
    api_ace = 0
    # 開始進行爬蟲
    # 使用API測試現在是否為開獎時間
    try:
        needtocrawl, api_qs = readytocrawl("sd11x5")
    except Exception as e:
        print('readtocrawl 失敗::',str(e))
        needtocrawl = True
        api_qs = '0'
    if needtocrawl:
        # 資料庫無開獎資料的情況
        ## 啟動所有來源進行爬蟲
        cores = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(processes=cores)
        for i in range(1,spider_num+1):
            cust_fun_name = 'spyder_'+str(i)
            cust_func = mapping[cust_fun_name]
            result1 = pool.apply_async(cust_func)
            aresults.append(result1)
        # result1 = pool.apply_async(spyder_1)
        # aresults.append(result1)
        pool.close()
        pool.join()
        results = [aresults[i].get() for i in range(0, len(aresults)) ]
        print('收集的開獎結果:::',results)
    else: 
        # 已開獎情況，只做檢查對方有無先開獎
        print("結果已開獎，無須處理開獎，只進行期數比對!")
        # 隨機取幾支爬蟲
        sel_num = 1
        # 從爬蟲範圍隨機取得設定數量的爬蟲
        dolist = random.sample(range(1,spider_num), sel_num)
        cores = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(processes=cores)
        for i in range(0,sel_num):
            cust_fun_name = 'spyder_'+str(dolist[i])
            cust_func = mapping[cust_fun_name]
            result1 = pool.apply_async(cust_func)
            aresults.append(result1)
        pool.close()
        pool.join()
        results = [aresults[i].get() for i in range(0, len(aresults)) ]
        print('收集的開獎結果:::',results)
    
    # 開獎時間
    if needtocrawl:
        # 期數, 開獎結果拆成兩個陣列個別處理
        m_period = []
        m_numbs = []
        for elem in results:
            if elem != None:
                m_period.append(elem['periods'])
                m_numbs.append(str(elem['numbers']))
        # 找出最多一樣的結果 or API讀取錯誤的話就抓最多出現期數
        most_key = ''
        ready_key = ''
        for key, value in Counter(m_period).most_common(1):
            most_key = key
            print('最多開獎期數::',key,'::',value)
        # ================================================
        try:
            if api_qs == db.check_sql_toopen("sd11x5",False,'YMD'):
                ready_key = api_qs
            else:
                # 如果資料庫比對失敗就找最多開獎結果
                print('資料庫跟API對照失敗!!!')
                ready_key = most_key
        except Exception as e:
            ready_key = most_key


        # 找出指定期數(在此是最多相同結果)的開獎號碼
        get_re = find_most_comm(ready_key,results,"sd11x5")
        real_list = get_re['real_list']
        to_retry = get_re['to_retry']
        # 到此來源比較結束==================================

        if to_retry == 0 :
            print("最終開獎::",ready_key, '::',real_list)
            sd11x5_item['periods'] = ready_key
            sd11x5_item['winning_numbers'] = real_list
            db.update_db(con='sd11x5',item=sd11x5_item)
            # check_sql_prizedata('sd11x5' , str(ready_key), real_list)
        # 檢驗資料庫有無資料
        # db.check_sql_updated(con='sd11x5',qishu=sd11x5_item['periods'])
        # 有比資料庫預期希望爬的期數更新的資料的話，就通知MQtt警報
        elif to_retry ==1 and retry > 0:
            # 如果獲取能比對的結果過少(少於兩筆), 則重新獲取
            print("剩餘重試次數:::", retry)
            time.sleep(random.randint(8, 18))
            main((retry-1))
    # 非開獎時間
    else:
        try:
            ready_key = gtpd('sd11x5' , 5)
            print("API要爬的期數::",ready_key)
        except Exception as e:
            ready_key = '0'
            print("API抓取預備開獎期數錯誤!")
    # 正常要爬蟲的if End==================================================
    # 判斷有無期數超於預期，有的話可能表示我們的封盤時間太晚
    for elem in results:
        if elem != None and ready_key != '0' and int(elem['periods']) > int(ready_key):
            msg = 'sd11x5彩種:: 爬蟲偵測到有新的期數::' + str(elem['periods'])
            alarm_messages(msg)
            print('有更新的期數')
    print("Process end!")
    
if __name__ == '__main__':
    # 帶入2次重試次數
    main(1)
        
