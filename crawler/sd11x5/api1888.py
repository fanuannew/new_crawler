from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    ln11x5 = {}
    url = "http://18881111.com/getDayData.ashx?lotCode=10008"
    resp = requests.get(url=url,timeout=10)
    jsontext = json.loads(resp.text)
    errorCode = jsontext['errorCode']
    if errorCode == 0:
        periods = str(jsontext['result']['data'][0]['preDrawIssue'])
        ymd = "20" + periods[:6]
        num = periods[-2:]
        ln11x5['periods'] = ymd + '0' + num

        get_nums = jsontext['result']['data'][0]['preDrawCode']
        num_list = [str(x) for x in get_nums.split(',')]
        ln11x5['numbers'] = list(map(str, num_list))

    print(url[:20],ln11x5)
    return ln11x5

if __name__ == "__main__":
    crawler()