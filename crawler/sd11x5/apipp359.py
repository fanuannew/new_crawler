from bs4 import BeautifulSoup
import requests
import re, json

def crawler():
    ln11x5 = {}
    headers = {
                'Content-Type': 'text/html; charset=UTF-8',
                'Content-Encoding': 'gzip',
                'Connection' : 'keep-alive',
                'Host' :'359015.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0'
            }
    s = requests.session()
    # s.get(url="http://359015.com/",headers=headers,timeout=5)
    url = 'http://359015.com/webapi/base_getawarddata?code=sd11x5'
    resp = s.get(url=url,headers=headers,timeout=10)
    # cookies = requests.utils.dict_from_cookiejar(s.cookies)
    # print(resp.text)
    jsontext = resp.json()
    if jsontext['status'] == 200:
        periods = str(jsontext['data']['current']['period'])
        ymd = periods[:8]
        num = periods[-2:]
        ln11x5['periods'] = ymd + '0' + num

        get_nums = jsontext['data']['current']['result']
        num_list = ['{0:0>2}'.format(str(x)) for x in get_nums.split(',')]
        ln11x5['numbers'] = num_list
    
    
    s.close()
    print(url[:20],ln11x5)
    return ln11x5

if __name__ == "__main__":
    crawler()