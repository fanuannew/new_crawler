from bs4 import BeautifulSoup
import requests
import json, random, datetime

def crawler():
    fc3d_item = {}
    rndnum = str(random.random())
    # ggperiod = str(datetime.datetime.today().strftime('%Y%m%d')) + '{0:0>3}'.format(random.randint(1,75))
    url = "https://www.1393c.com/syydj/ajax?ajaxhandler=GetAwardData&t=" + rndnum
    # print(url)
    # payload = {'category':'sh11x5'}
    resp = requests.get(url=url,timeout=6)
    jsontext = json.loads(resp.text)
    if len(str(jsontext))>5:
        periods = str(jsontext['current']['period'])
        ymd = periods[:8]
        num = periods[-2:]
        fc3d_item['periods'] = ymd + '0' + num

        get_nums = str(jsontext['current']['awardNumbers']).split(',')
        fc3d_item['numbers'] = list(map('{0:0>2}'.format, get_nums))

    print(url[:20],fc3d_item)
    return fc3d_item

if __name__ == "__main__":
    crawler()