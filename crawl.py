import sys, getopt
import datetime
def main_crawl(argv):
   crawlto = ''
   try:
      opts, args = getopt.getopt(argv,"hc:",["cfile="])
   except getopt.GetoptError:
      print('test.py -i <inputfile> -o <outputfile>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print('crawl.py -c [彩種名稱]')
         sys.exit()
      elif opt in ("-c", "--cfile"):
         crawlto = arg

   print('現在時間:', datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
   print('準備要爬蟲:', crawlto)
   
   # 帶入3次重試次數
   if crawlto == "bjpc28":
      myfunc = getattr(__import__('crawler.bjpc28.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "xj11x5":
      myfunc = getattr(__import__('crawler.xj11x5.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "ln11x5":
      myfunc = getattr(__import__('crawler.ln11x5.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "sd11x5":
      myfunc = getattr(__import__('crawler.sd11x5.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "heb11x5":
      myfunc = getattr(__import__('crawler.heb11x5.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "hlj11x5":
      myfunc = getattr(__import__('crawler.hlj11x5.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "sh11x5":
      myfunc = getattr(__import__('crawler.sh11x5.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "jx11x5":
      myfunc = getattr(__import__('crawler.jx11x5.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "js11x5":
      myfunc = getattr(__import__('crawler.js11x5.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "hkmk6":
      myfunc = getattr(__import__('crawler.hkmk6.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "fc3d":
      myfunc = getattr(__import__('crawler.fc3d.main', fromlist=["main"]), 'main')
      myfunc(1)
   elif crawlto == "pl3":
      myfunc = getattr(__import__('crawler.pl3.main', fromlist=["main"]), 'main')
      myfunc(1)
   else:
      print("-c 後面參數錯誤，無此彩種!")


if __name__ == "__main__":
   main_crawl(sys.argv[1:])